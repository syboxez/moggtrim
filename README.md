moggtrim is a program that will remove x seconds of sound from the beginning of an mogg file

moggtrim depends on:
 - ffmpeg
 - ogg2mogg (Available at https://github.com/mtolly/ogg2mogg)

vgstrim depends on:
 - sox
 - rockaudio (for vgstrim) (Available at https://www.walkingshadows.org/projects/rocklib/index.php)

All of these dependencies must be in your PATH
